package br.nom.penha.bruno.watertank.monitor;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pi4j.wiringpi.Serial;

@Stateless
@Path(value = "/coletorREST")
public class ColetorREST {

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getMonitor() {

		System.out.println("<--Pi4J--> preparando conexao SERIAL para envio");

		// Abre a porta serial para comunicação
		int fd = Serial.serialOpen(Serial.DEFAULT_COM_PORT, 9600);
		if (fd == -1) {
			System.out.println(" ==>> CONFIGURACAO SERIAL FALHOU");
		}

		// loop infinito
		char dadoRecebido = '|';
		while (true) {

			// envia uma mensagem ASCII
			// Serial.serialPuts(fd, "TEST\r\n");

			// acumula o dado recebido para enviar
			int dadoDisponivel = Serial.serialDataAvail(fd);
			if (dadoDisponivel > 0) {
				int dado = Serial.serialGetchar(fd);
				dadoRecebido += (char) dado;
				dadoDisponivel = Serial.serialDataAvail(fd);
			}else{
				break;
			}

			
		}

		return String.valueOf(dadoRecebido);

	}

	@GET
	@Produces(value = "text/plain")
	@Path(value = "{id}")
	public String getPropety(@PathParam("id") int id) {
		String retorno = "Recebido " + id;
		System.out.println(retorno);
		return retorno;
	}

	@POST
	@Produces(value = "text/html")
	@Path(value = "form")
	public String handlePost(@PathParam("name") String name,
			@PathParam("age") int age) {
		return "<html><body><p>name: " + name + "<p>age: " + age;
	}
}
