package br.nom.penha.bruno.watertank.monitor;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class ColetorRESTApp extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> set = new HashSet<Class<?>>();
        set.add(ColetorREST.class);
        return set;
    }

}
