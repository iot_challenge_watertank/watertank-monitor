package br.nom.penha.bruno.watertank.monitor;

import com.pi4j.wiringpi.Serial;

public class ExemploConectandoGPIO {
    
    public static void main(String args[]) throws InterruptedException {
        
        System.out.println("<--Pi4J--> SERIAL programa de teste");

        // open serial port for communication
        int fd = Serial.serialOpen(Serial.DEFAULT_COM_PORT, 9600);
        if (fd == -1) {
            System.out.println(" ==>> CONFIGURACAO SERIAL FALHOU");
            return;
        }

        // infinite loops
        while(true) {
            
            // send test ASCII message
            //Serial.serialPuts(fd, "TEST\r\n");

            // display data received to console
            int dataavail = Serial.serialDataAvail(fd);            
            while(dataavail > 0) {
                int data = Serial.serialGetchar(fd);
                System.out.print((char)data);                
                dataavail = Serial.serialDataAvail(fd);
            }
            
            // wash, rinse, repeat
            Thread.sleep(1000);
        }
    }
}